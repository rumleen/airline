/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package airline;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Flight {
    private Integer flight_number;
    private String flight_date;

    public Flight(Integer flight_number, String flight_date) {
        this.flight_number = flight_number;
        this.flight_date = flight_date;
    }

    public Integer getFlight_number() {
        return flight_number;
    }

    public void setFlight_number(Integer flight_number) {
        this.flight_number = flight_number;
    }

    public String getFlight_date() {
        return flight_date;
    }

    public void setFlight_date(String flight_date) {
        this.flight_date = flight_date;
    }

    public Flight() {
        super();
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "Airline{" +
                "flight_number=" + flight_number +
                ", flight_date='" + flight_date + '\'' +
                '}';
    }
}
