/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package airline;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Airport {
    private Integer airport_id;
    private Character airport_code;

    public Airport(Integer airport_id, Character airport_code) {
        this.airport_id = airport_id;
        this.airport_code = airport_code;
    }

    public Character getAirport_code() {
        return airport_code;
    }

    public void setAirport_code(Character airport_code) {
        this.airport_code = airport_code;
    }

    public Integer getAirport_id() {
        return airport_id;
    }

    public void setAirport_id(Integer airport_id) {
        this.airport_id = airport_id;
    }

    @Override
    public String toString() {
        return "Airline{" +
                "airport_id=" + airport_id +
                ", airport_code=" + airport_code +
                '}';
    }
}
