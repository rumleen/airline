/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package airline;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Airplane {
    private Long plane_ID;
    private String make;
    private String model;

    public Airplane(Long plane_ID, String make, String model) {
        this.plane_ID = plane_ID;
        this.make = make;
        this.model = model;
    }

    public Long getPlane_ID() {
        return plane_ID;
    }

    public void setPlane_ID(Long plane_ID) {
        this.plane_ID = plane_ID;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
    @Override
    public String toString() {
        return "Airline{" +
                "plane_ID=" + plane_ID +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
