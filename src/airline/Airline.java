/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package airline;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Airline {

    private Long airline_id;
    private Character airline_Name;
    private String airline_code;
    
    public Airline(Long airline_id, Character airline_name, String airline_code) {
        this.airline_id = airline_id;
        airline_Name = airline_name;
        this.airline_code = airline_code;
    }
    public Long getAirline_id() {
        return airline_id;
    }

    public void setAirline_id(Long airline_id) {
        this.airline_id = airline_id;
    }

    public Character getAirline_Name() {
        return airline_Name;
    }

    public void setAirline_Name(Character airline_Name) {
        this.airline_Name = airline_Name;
    }

    public String getAirline_code() {
        return airline_code;
    }

    public void setAirline_code(String airline_code) {
        this.airline_code = airline_code;
    }
    @Override
    public String toString() {
        return "Airline{" +
                "airline_id=" + airline_id +
                ", airline_Name=" + airline_Name +
                ", airline_code='" + airline_code + '\'' +
                '}';
    }
     
}
